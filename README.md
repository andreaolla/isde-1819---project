# ISDe 2018-2019 - Project Assignment

Design and implement an **image processing toolbox** 
that allows the user to select a set of image transformations.
The project will be evaluated based on the following three main aspects: 
1) the use of gitlab, to monitor and maintain the project;
2) the quality of the produced documentation and tests; and
3) the use of proper design patterns to implement 
the required functionality.


**Gitlab.** Use Gitlab to track progress and changes in 
project implementation, and to run integrated tests. 
Define a role for each member of your team,
assign them issues, and use the Git board to manage 
and monitor the state of the project.
Define milestones corresponding to earlier versions of the project
(e.g., `v0.1`, `v0.2`, `v0.3`) up to the final release (`v1.0`).
Use a master and a develop branch. Motivate and define which issues have to be addressed in each milestone.
Each team developer should address the assigned issue by creating a proper branch and merge request.
The team leader is then responsible to validate the code and merge the request in the development branch,
and then in the master branch. The analysis of the Gitlab documentation must show clearly 
individual contributions.

**Documentation and Testing**. Use *sphynx* to automatically generate the documentation
of your project using docstrings. The documentation has to be stored in HTML format.
Design suitable unit tests for each class / function
in your code.
These will be evaluated based on coverage and on the creativity and structure of the testing infrastructure.
Please ensure that all tests are correctly integrated with the Gitlab's continuous
integration (CI) mechanism, and that they are always passed before integrating any merge request in the develop
and master branch.  

**Software Implementation and Design Patterns.** 
This project can be implemented using some of the design patterns we have 
discussed throughout the course.
This part will be evaluated based on the quality of the produced code,
 its algorithmic correctness, and on the proper choice of the design patterns
 selected to implement a given functionality.
     


## Image Processing Toolbox - Implementation Specifics
 
### Program Flow and User Interface

The image processing toolbox has to implement different image filtering 
techniques, including blur, sharpen and many others.
It should work as depicted from the following state diagram:

![](resources/images/state.png)


**START.** The user selects an image from the disk, the image is loaded and the 
program moves to the state `SELECT`.

**SELECT.** The user can select a filtering technique, among the implemented ones, 
and move to `PROCESS`. Alternatively, the image can be reset, and the program goes back to
`START`.

**PROCESS.** Here the user can decide to : (i) go back to state `START`, 
to select a different image; (ii) go back to state `SELECT`, to select 
a different filtering technique; 
(iii) apply the selected filtering technique (possibly multiple times);
or (iv) save the current image to disk (possibly multiple times). 

From any state, the program must allow the user to exit and terminate the program.

*Example of user interface.* Here is a simple example of what the user interface 
may display on the terminal, based on the program state.

- State `START`:
    - Load Image
    - Quit

- State `SELECT`:
    - Set Filter: Blur
    - Set Filter: Sharpen
    - ...
    - Reset    
    - Quit

- State `PROCESS`:
    - Apply function
    - Save image
    - Reset
    - Quit



It is warmly recommended to separate the model core functionality
from the program interface, using, e.g., the model-view-controller 
design pattern.

### Image Filtering

**Image Filters**. The toolbox should implement the following filtering techniques:
1. blur (box and gaussian);
2. sharpen;
3. edge detection (horizontal and vertical).
The aforementioned filters should be implemented 
in two versions, using a 3x3-kernel mask and a 5x5-kernel mask.
 
*Optional:* Implement the Sobel edge detector, the Canny edge detector, 
or any other filtering technique of your choice.

*Hint:* these are all convolutional filters, so it is recommended to define
a single class for such family of filters, and use the strategy pattern to
associate a given kernel (convolution mask) to it.
For example, one may thus define `blur = CFilterConv('blur')`, being `blur` the
name of the given kernel to be selected from the strategy pattern.

Finally, please define an additional "combo" filter given by the 
combination of three of the given filters of your choice (applied in sequence).
















