import unittest
import controller.filters as flt
import numpy as np


class TestFilters(unittest.TestCase):

    def test_scalar_product(self, n, n_expected):
        self.assertEqual(type(n), np.int64)

        self.assertEqual(n, n_expected)

    def test_apply_mask(self, img_processed, img_expected):
        self.assertEqual(type(img_processed), np.ndarray)
        self.assertEqual(img_processed.shape, img_expected.shape)

        self.assertTrue((img_processed == img_expected).all)

    def test_kernel_generator(self, kernel, kernel_expected):
        self.assertEqual(type(kernel), np.ndarray)
        self.assertEqual(kernel.shape, kernel_expected.shape)

        self.assertTrue((kernel == kernel_expected).all)

    def test_sobel_kernel_generator(self, kernels, kernel_expected):
        self.assertEqual(type(kernels), list)

        self.assertEqual(type(kernels[0]), np.ndarray)
        self.assertEqual(type(kernels[1]), np.ndarray)

        self.assertEqual(kernels[0].shape, kernel_expected[0].shape)
        self.assertEqual(kernels[1].shape, kernel_expected[1].shape)

        self.assertTrue((kernels[0] == kernel_expected[0]).all)
        self.assertTrue((kernels[1] == kernel_expected[1]).all)

    def test_process(self, image_processed, img_expected):
        self.assertEqual(type(image_processed), np.ndarray)
        self.assertEqual(image_processed.shape, img_expected.shape)
        self.assertTrue((image_processed[0] == img_expected[0]).all)


if __name__ == '__main__':
    test_filters = TestFilters()
    ones_matrix = np.ones((250, 250), int)

    ones_img = np.ones((250, 250, 3))
    zeros_image = np.zeros((250, 250, 3))

    ones_3x3 = np.ones((3, 3), int)
    zeros_3x3 = np.zeros((3, 3), int)

    print("Testing `scalar_product`...")

    n = flt.convolution_product(ones_3x3, ones_3x3)
    test_filters.test_scalar_product(n, 9)
    n = flt.convolution_product(zeros_3x3, zeros_3x3)
    test_filters.test_scalar_product(n, 0)

    print("Testing `apply_mask`...")
    kernel = ones_3x3 / 9
    img_processed = flt.apply_mask(kernel, ones_matrix)
    img_expected = ones_matrix
    test_filters.test_apply_mask(img_processed, img_expected)

    kernel = ones_3x3 / 9
    img_processed = flt.apply_mask(kernel, ones_matrix)
    test_filters.test_apply_mask(img_processed, np.zeros(ones_matrix.shape))

    print("Testing `kernel_generator`...")
    # Test for kernerl generator of BlurFilter
    kernel = flt.BlurFilter(3).kernel
    kernel_expected = ones_3x3
    test_filters.test_kernel_generator(kernel, kernel_expected)

    # Test for kernerl generator of BoxBlurFilter
    kernel = flt.BlurFilter(3).kernel
    kernel_expected = ones_3x3 / 9
    test_filters.test_kernel_generator(kernel, kernel_expected)

    # Test for kernerl generator of EdgeDetectionFilter
    kernel = flt.EdgeDetectionFilter(3).kernel
    kernel_expected = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
    test_filters.test_kernel_generator(kernel, kernel_expected)

    # Test for kernerl generator of SharpenFilter
    kernel = flt.SharpenFilter(3).kernel
    kernel_expected = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
    test_filters.test_kernel_generator(kernel, kernel_expected)

    print("Testing `kernel_generator` of SobelEdgeFilter...")
    # Test for kernerl generator of SobelEdgeFilter
    kernels = flt.SobelEdgeFilter(3).kernel
    kernel_expected = [np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]]), np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])]
    test_filters.test_sobel_kernel_generator(kernels, kernel_expected)

    print("Testing `process` for BlurFilter...")
    # Test one for process of Blur:
    img = ones_img
    image_processed = flt.BlurFilter(3).process(img)
    img_expected = img * 9
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of Blur
    img = zeros_image
    image_processed = flt.BlurFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    print("Testing `process` for BoxBlurFilter...")
    # Test one for process of BoxBlur:
    img = ones_img
    image_processed = flt.BoxBlurFilter(3).process(img)
    img_expected = ones_img
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of boxBlur
    img = zeros_image
    image_processed = flt.BoxBlurFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    print("Testing `process` for GaussianBlurFilter...")
    # Test one for process of GaussianBlur:
    img = zeros_image
    image_processed = flt.GaussianBlurFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of GaussianBlur:
    img = ones_img
    image_processed = flt.GaussianBlurFilter(3).process(img)
    img_expected = ones_img
    test_filters.test_process(image_processed, img_expected)

    print("Testing `process` for EdgeDetectionFilter...")
    # Test one for process of EdgeDetectionBlur:
    img = zeros_image
    image_processed = flt.EdgeDetectionFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of EdgeDetectionBlur:
    img = ones_img
    image_processed = flt.EdgeDetectionFilter(3).process(img)
    img_expected = ones_img
    test_filters.test_process(image_processed, img_expected)

    print("Testing `process` for SharpenFilter...")
    # Test one for process of SharpenBlur:
    img = zeros_image
    image_processed = flt.SharpenFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of SharpenBlur:
    img = ones_img
    image_processed = flt.SharpenFilter(3).process(img)
    img_expected = ones_img
    test_filters.test_process(image_processed, img_expected)

    print("Testing `process` for SobelFilter...")
    # Test one for process of SobelBlur:
    img = zeros_image
    image_processed = flt.SobelEdgeFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    # Test two for process of SobelBlur:
    img = ones_img
    image_processed = flt.SobelEdgeFilter(3).process(img)
    img_expected = zeros_image
    test_filters.test_process(image_processed, img_expected)

    print("All tests successful!")
