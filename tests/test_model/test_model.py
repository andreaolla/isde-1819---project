import unittest
from model.fun_utils import load, save
import cv2
from tests.utils.test_img import fake_img


class TestModelClass(unittest.TestCase):

    def test_load_wrong_path(self):
        self.assertIsNone(load('wrong path'))

    def test_save_wrong_path(self):
        with self.assertRaises(cv2.error):
            save('wrong_path', fake_img())


if __name__ == '__main__':
    unittest.main()
