import numpy as np


def fake_img():
    img = np.zeros([5, 5, 3])
    img[:, :, 0] = np.ones([5, 5]) * 64 / 255.0
    img[:, :, 1] = np.ones([5, 5]) * 128 / 255.0
    img[:, :, 2] = np.ones([5, 5]) * 192 / 255.0
    return img
