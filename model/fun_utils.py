import cv2


def load(path_name):
    """
    \n**General description of the method:**

    \nthis method takes as input the path of the file and the filename plus .extension and returns the image as a numpy array



    +---------------------------------+
    |**Input**: *path_name*           |
    +=================================+
    |                                 |
    |  **path_name type:** ``string`` |
    |                                 |
    |                                 |
    +---------------------------------+

    +-------------------------------+
    |**Output**: *img*              |
    +===============================+
    |                               |
    |  **img type:** ``np.array``   |
    +-------------------------------+
    """
    img = cv2.imread(path_name)

    return img


def save(path_name, img):
    """
    \n**General description of the method:**

    \nthe method save take as input the path name where the user want to save the image and the image itself

    +--------------------------------+
    |**Input**: *pathname*, *img*    |
    +================================+
    |                                |
    |  **path_name type:** ``string``|
    |                                |
    |  **img type:** ``np.array``    |
    +--------------------------------+

    """
    try:
        cv2.imwrite(path_name, img)
    except cv2.error:
        raise cv2.error
