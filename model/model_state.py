from abc import ABC, abstractmethod
from controller.controller import Controller as Cc
from model.fun_utils import load, save
import cv2

"""
The model_state class permits to relegate to the Controller the execution of actions decided by the user.
\nIt uses state pattern to differentiate the menu behavior according to the program status.
"""


class Context:
    """
    \nThis class permits to change the current state object in order to execute different behaviors.
    """

    def __init__(self, state):
        """
        \n**General description of the method:**

        \nthis method takes as input the current state object and store it as an attribute of the Context class.



        +-------------------------------+
        |**Input**: *state*             |
        +===============================+
        |                               |
        |  **state type:** ``obj``      |
        |                               |
        |                               |
        +-------------------------------+
        """
        self._state = state

    def request(self):
        """
        \n**General description of the method:**

        \nthis method recall the method handle (implemented as an abstract method in abstract class State) that permit
        \nto change behavior according to the current state object.
        """

        self._state.handle()


class State(ABC):
    """
    \nThis abstract class implement the interface of State classes using an abstract :meth:'handle' that define in
    \ninherited classes the desired behavior of the chosen state.

    """

    @abstractmethod
    def handle(self):
        pass


class Start(State):
    """
    \nThis inherited class represent the start behavior.
    """

    def handle(self):

        """
        \n**General description of the method:**

        \nThis method apply :func:`.start` of :class:`.Controller` and store on the variable command the input chosen
        \nby user. Depending on the value of command this method permit to:
        \n1.Recall :func:`.load_input` from :class:`.Controller` obtaining from the user the path of the image and
        \napply :func:`.load` in order to load the image. After jump to select filter class behavior.
        \n2.Jump to quit class.
        \n3.Reset the execution.

        """
        command = Cc.start()

        if command == "1":
            image_path = Cc.load_input()
            loaded_image = load(image_path)

            if loaded_image is None:
                Cc.load_read_exception()
                jump_to_start()
            else:
                jump_to_select_filter(loaded_image, [])  # It load

        elif command == "2":
            jump_to_quit()
        else:
            Cc.exception()
            jump_to_start()


class SelectFilter(State):
    """
    \nThis inehrited class represent the select filter behavior.
    """

    def __init__(self, img, filters):
        """
        \n**General description of the method:**

        \nThis method takes as input the loaded image and store it as an attribute of :class:`.SelectFilter`

        +-------------------------------+
        |**Input**: *img*               |
        +===============================+
        |                               |
        |**img type:** ``numpy.ndarray``|
        |                               |
        |                               |
        +-------------------------------+
        """
        self.img = img
        self.filters = filters

    def handle(self):

        """
        \n**General description of the method:**

        \nThis method apply :func:`.set_filter` of :class:`.Controller` and store on the variable filter_type the input
        \nchosen by user. Depending on the value of filter_type this method permit to:
        \n1,2,3. Jump to select intensity (filters in which the user can choose the kernel_dimension)
        \n4,5,6. Instantiate an object filter of controller class. (filters in which kernel_dimension is fixed at three.
        \n7.Reset the execution.
        \n8.Jump to quit class.
        \nIf no one of those statement is respected the user is invited to type a correct options by recalling
        \n:func:`.exception` from :class:`Controller`.
        """
        Cc.select_the_filter_number(len(self.filters) + 1)
        filter_type = Cc.set_filter()

        if filter_type == "0" or filter_type == "1" or filter_type == "2" or filter_type == "3" or filter_type == "4" or filter_type == "8":
            jump_to_select_intensity(self.img, int(filter_type), self.filters)

        elif filter_type == "5" or filter_type == "6" or filter_type == "7" or filter_type == "9":
            self.filters.append(Cc.filter(int(filter_type), 3))

            another_filter = Cc.view_another_filter()
            if another_filter == "y":
                jump_to_select_filter(self.img, self.filters)
            elif another_filter == 'n':
                jump_to_process(self.filters, self.img)

        elif filter_type == "10":
            jump_to_start()

        elif filter_type == "11":
            jump_to_quit()

        else:
            Cc.exception()
            jump_to_select_filter(self.img, self.filters)


class SelectIntensity(State):
    """
    \nThis inehrited class represent the select intensity behavior.
    """

    def __init__(self, img, filter_type, filters):

        """
        \n**General description of the method:**

        \nThis method takes as input the loaded image and the filter_type and stores them as  attributes of the
        \nSelectIntensity class.

        +-------------------------------+
        |**Input**: *img*, *filter_type*|
        +===============================+
        |                               |
        |**img type:** ``numpy.ndarray``|
        |                               |
        |**filter_type type:** ``int``  |
        |                               |
        +-------------------------------+
         """

        self.img = img
        self.filter_type = filter_type
        self.filters = filters

    def handle(self):

        """
        \n**General description of the method:**

        \nThis method permit to set the intensity of the filter by choosing three different size of kernel_dimension:
        \n3,5 or 7.
        """

        intensity = Cc.select_intensity()
        if intensity != 3 and intensity != 5 and intensity != 7:
            Cc.exception()
            jump_to_select_intensity(self.img, self.filter_type, self.filters)

        elif 0 <= self.filter_type <= 9:
            self.filters.append(Cc.filter(self.filter_type, intensity))

            another_filter = Cc.view_another_filter()
            if another_filter == "y":
                jump_to_select_filter(self.img, self.filters)
            elif another_filter == 'n':
                jump_to_process(self.filters, self.img)

        else:
            Cc.exception()
            jump_to_select_filter(self.img, self.filters)


class Process(State):
    """
    \nThis inehrited class represent the process behavior.
    """

    def __init__(self, filter_list, image):
        """
        \n**General description of the method:**

        \nIt takes as input the filter to apply and an img and stores them as attributes of the Process class.

        +-----------------------------------------------+
        |**Input**: *filter_to_apply*, *processed_image*|
        +===============================================+
        |                                               |
        |**filter_to_apply type:** ``obj``              |
        |                                               |
        |**processed_image type:** ``numpy.ndarray``    |
        |                                               |
        +-----------------------------------------------+
        """

        self.filters = filter_list
        self.image = image

    def handle(self):

        """
        \n**General description of the method:**

        \nThis method apply the method :func:`.process` of :class:`.Controller`  and store on the variable command the input
        \nchosen by user. Depending on the value of command this method permit to:
        \n1. Process the image calling the method process of the chosen filter class.
        \n2. Call :func:`.save_input` from :class:`.Controller` and stores the input chosen by the user in the variable path. After save
        \nthe image using :func:`.save` and jump to process state.
        \n3. Reset the execution.
        \n4. Plot the processed_image and jump to process.
        \n5. Jump to quit class.
        \nIf no one of those statement is respected the user is invited to type a correct options by recalling
        \n:func:`.exception` from :class:`.Controller`
        """
        command = Cc.process()

        if command == "1":
            for filter in self.filters:
                self.image = Cc.process_caller(filter, self.image)

            jump_to_process(self.filters, self.image)

        elif command == "2":
            path = Cc.save_input()
            try:
                save(path, self.image)
                Cc.correctly_saved()
            except cv2.error:
                Cc.load_read_exception()
            jump_to_process(self.filters, self.image)

        elif command == "3":
            jump_to_start()

        elif command == "4":

            Cc.plot(self.image)
            jump_to_process(self.filters, self.image)

        elif command == "5":
            jump_to_quit()

        else:
            Cc.exception()
            jump_to_process(self.filters, self.image)


class Quit(State):
    """
    \nThis class represent the quit behavior.
    """

    def handle(self):
        """
        \n**General description of the method:**

        \nThis method call :func:`.quit` from :class:`.Controller`.
        """
        Cc.quit()


def jump_to_start():
    start = Start()
    context = Context(start)
    context.request()


def jump_to_select_filter(img, filters_list):
    select_filter = SelectFilter(img, filters_list)
    context = Context(select_filter)
    context.request()


def jump_to_select_intensity(img, filter_type, filters_list):
    select_intensity = SelectIntensity(img, filter_type, filters_list)
    context = Context(select_intensity)
    context.request()


def jump_to_process(filter, image):
    process = Process(filter, image)
    context = Context(process)
    context.request()


def jump_to_quit():
    quit = Quit()
    context = Context(quit)
    context.request()


if __name__ == "__main__":
    jump_to_start()
