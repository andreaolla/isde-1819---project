class View:
    """
    This class connect the user interface, that implements the menu with the possibility of choose actions, with the controller.
    """

    @staticmethod
    def view_start():
        """
        \n**General description of the method:**

        \nThis method introduce the user to the first part of menu, giving the possibility to load an image or to quit by the program.

        +-------------------------------+
        |**Output**: *command*          |
        +===============================+
        |**command type**: ``str``      |
        +-------------------------------+
        """

        command = input("START:\n\n"
                        "1. Load Image\n"
                        "2. Quit\n")
        return command

    @staticmethod
    def view_set_filter():
        """
        \n**General description of the method:**
        \nThis method introduce the user to the second part of menu, giving the possibility to set the desired filter,
        \nto go back to the first part of the menu or to quit by the program.

        +-------------------------------+
        |**Output**: *filter_type*      |
        +===============================+
        |**filter_type type**: ``str``  |
        +-------------------------------+
        """

        filter_type = input("SELECT:\n\n"
                            "0. Set filter: BLUR\n"
                            "1. Set filter: BOX BLUR\n"
                            "2. Set filter: GAUSSIAN BLUR\n"
                            "3. Set filter: HORIZONTAL DETECTION\n"
                            "4. Set filter: VERTICAL DETECTION\n"
                            "5. Set filter: SHARPEN\n"
                            "6. Set filter: SOBEL\n"
                            "7. Set filter: EDGE DETECTION\n"
                            "8. Set filter: MEDIAN\n"
                            "9. Set filter: BINARIZATION\n"
                            "10. Reset\n"
                            "11. Quit\n\n ")
        return filter_type

    @staticmethod
    def view_load():
        """
        \n**General description of the method:**
        \nThis method, in the case the user decide to load an image, permit to receive as input the path of the image.
        \nIt's recalled in the controller_state module.

        +-------------------------------+
        |**Output**: *image_path*       |
        +===============================+
        |**image_path type**: ``str``   |
        +-------------------------------+
        """

        image_path = input("\nPlease, insert the path of the image you need to load.\n")
        return image_path

    @staticmethod
    def view_select_intensity():
        """
        \n**General description of the method:**
        \nThis method introduce the user to the third part of menu, giving the possibility to select the intensity of the selected filter.

        +-------------------------------+
        |**Output**: *intensity*        |
        +===============================+
        |**intensity type**: ``str``    |
        +-------------------------------+
        """

        intensity = input("INTENSITY OF THE FILTER:\n\n"
                          "1. Low\n"
                          "2. Medium\n"
                          "3. High\n")
        return intensity

    @staticmethod
    def view_process():
        """
        \n**General description of the method:**
        \nThis method introduce the user to the fourth part of menu, giving the possibility to apply the selected filter to the loaded image, to save it,
        \nto go back to the first part of menu,to plot the image and to quit by the program.

        +-------------------------------+
        |**Output**: *command*          |
        +===============================+
        |**command type**: ``str``      |
        +-------------------------------+
        """

        command = input("SELECT:\n\n"
                        "1. Apply function\n"
                        "2. Save image\n"
                        "3. Reset\n"
                        "4. Plot\n"
                        "5. Quit\n")
        return command

    @staticmethod
    def view_save():
        """
        \n**General description of the method:**
        \nThis method, in the case the user decide to save an image, permit to receive as input the path where the user want to store the image.
        \nIt's recalled in the controller_state module.

        +-------------------------------+
        |**Output**: *image_path*       |
        +===============================+
        |**image_path type**: ``str``   |
        +-------------------------------+
        """
        image_path = input("\nPlease write the path where you want to save you image:\n")
        return image_path

    @staticmethod
    def view_quit():
        """
        \n**General description of the method:**
        \nThis method print a notice of correct exit from the program.
        """

        print("\nYou correctly quitted.")

    @staticmethod
    def view_exception():
        """
        \n**General description of the method:**
        \nThis method print a notice of error in the chosen input.
        """
        print('\nPlease, digit one of the possibles actions.\n')

    @staticmethod
    def view_load_read_exception():
        """
        \n**General description of the method:**
        \nThis method print a notice of error in the chosen path.
        """
        print('\nImage format or path are not correct.\n')

    @staticmethod
    def view_correctly_saved():
        """
        \n**General description of the method:**
        \nThis method print a successful save message.
        """
        print('\nYou have correctly saved the image\n')

    @staticmethod
    def view_select_the_filter_number(n):
        """
        \n**General description of the method:**
        \nThis method ask what is the filter to apply.
        """
        print('\nSelect the filter number ' + str(n) + '\n')

    @staticmethod
    def view_another_filter():
        """
        \n**General description of the method:**
        \nThis method ask to the user if he desires to apply another filter.
        """
        return input('\nDo you want apply another filter? [y/n]\n')
