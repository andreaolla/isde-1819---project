from controller import filters as f
import cv2
from view.view import View
from matplotlib import pyplot as plt


class Controller(object):
    """
    this class is the link between the user interface and the model class. It uses the class filter in order
    to perform the process operation.
    """

    @staticmethod
    def filter(filter_type, intensity):
        """
        \n**General description of the method:**

        \nthis class instantiates a object of a user-specified class filter that performs the chosen elaboration. It takes as inputs:
        \n* filter type: given by the user can change the type of filter it will be applied
        \n* img: is the image that we want to process
        \n* intensity: can modify the dimension of the kernel

        +-----------------------------------------------+
        |**Input**: *filter_type*, *img*, intensity     |
        +===============================================+
        |                                               |
        |**filter_type type:** ``int``                  |
        |                                               |
        |**intensity type:** ``int``                    |
        +-----------------------------------------------+

        +-----------------------------------------------+
        |**Output**: *filter_type*, *img*, intensity    |
        +===============================================+
        |                                               |
        |**filter_type type:** ``int``                  |
        |                                               |
        |**img type:** ``np.array``                     |
        |                                               |
        |**intensity type:** ``int``                    |
        +-----------------------------------------------+


        """

        if filter_type == 0:
            filter = f.BlurFilter(intensity)

        elif filter_type == 1:
            filter = f.BoxBlurFilter(intensity)

        elif filter_type == 2:
            filter = f.GaussianBlurFilter(intensity)

        elif filter_type == 3:
            filter = f.HorizontalEdgeDetectionFilter(intensity)

        elif filter_type == 4:
            filter = f.VerticalEdgeDetectionFilter(intensity)

        elif filter_type == 5:
            filter = f.SharpenFilter(intensity)

        elif filter_type == 6:
            filter = f.SobelEdgeFilter(intensity)

        elif filter_type == 7:
            filter = f.EdgeDetectionFilter(intensity)

        elif filter_type == 8:
            filter = f.MedianFilter(intensity)

        elif filter_type == 9:
            filter = f.BinarizatorFilter(intensity)

        return filter

    @staticmethod
    def view_another_filter():
        answer = View.view_another_filter()
        if answer == "y" or answer == "n":
            return answer
        else:
            View.view_exception()
            return Controller.view_another_filter()

    @staticmethod
    def start():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_start`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """
        return View.view_start()

    @staticmethod
    def set_filter():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_set_filter`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_set_filter()

    @staticmethod
    def load_input():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_load`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_load()

    @staticmethod
    def select_intensity():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_select_intensity`. In case the user gives a wrong input it returns itself

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``int``      |
            |                                |
            +--------------------------------+

            """

        try:
            return int(View.view_select_intensity())*2 + 1
        except:
            View.view_exception()
            return Controller.select_intensity()

    @staticmethod
    def process():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_process`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_process()

    @staticmethod
    def save_input():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_save`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_save()

    @staticmethod
    def quit():
        View.view_quit()

    @staticmethod
    def exception():
        View.view_exception()

    @staticmethod
    def load_read_exception():
        View.view_load_read_exception()

    @staticmethod
    def correctly_saved():
        View.view_correctly_saved()

    @staticmethod
    def select_the_filter_number(n):
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_select_the_filter_number`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_select_the_filter_number(n)

    @staticmethod
    def another_filter():
        """
            \n**General description of the method:**

            \nit returns :meth:`view.View.view_another_filter`

            +--------------------------------+
            |**Output**: *output*            |
            +================================+
            |                                |
            |  **output type:** ``string``   |
            |                                |
            +--------------------------------+

            """

        return View.view_another_filter()

    @staticmethod
    def process_caller(filter, img):
        return filter.process(img)

    @staticmethod
    def plot(image):
        # plt.imshow((image * 255).astype(np.uint8))
        # plt.title('Your image processed:')
        # plt.show()

        cv2.imshow('image', image)
        cv2.waitKey(0)
