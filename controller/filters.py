from abc import abstractmethod, ABC
import numpy as np
from scipy import signal
import cv2

"""
Filters class belongs to Controller module, it takes care of the processing of the images.
In this module the image is processed through a convolution between the image and a specific kernel.
Each filter has its own kernel that is generated dynamically. It is possible to generate kernels with chosen dimension.

Each filter is a subclass of :class:`.Filters`.
"""


class Filters(ABC):
    """
    \nFilter class is the superclass of all the different filters classes. It implements the init method, process method and kernel_generator method as an abstract method.
    """

    def __init__(self, kernel_dimension):
        """
        \n**General description of the method:**

        \nIt take as inputs an rgb image as a numpy array with 3 layers, one for each primary color and the dimension of the kernel that it has to generate.

        \nIt generate three attribute:\n
        \n• img
        \n• kernel_dimension
        \n• kernel


        \nThe latest is generated dynamically through :meth:`kernel_generator`.


        +---------------------------------------+
        |**Input**: *img*, *kernel_dimension*   |
        +=======================================+
        |  **img type:** ``np.array``           |
        |                                       |
        |  **kernel_dimension type:** ``int``   |
        +---------------------------------------+

        """
        self.kernel_dimension = kernel_dimension
        self.kernel = self.kernel_generator()

    def process(self, image):
        """
        \n**General description of the method:**

        \nIt takes care of processing. When this method is called it apply the chosen filter to the image and it returns the processed image.

        \nIt splits the rgb image in three matrices (blue, green, red) one for each color.
        \nThen it applies the mask (kernel) on each matrix.
        \nFinally, it merges the channel and returns the processed image.\n

        +---------------------------------------+
        |**Input**: *image*                     |
        +=======================================+
        |  **image type:** ``np.array``         |
        +---------------------------------------+


        +-------------------------------------------+
        |**Output**: *img_processed*                |
        +===========================================+
        |  **image_processed type:** ``np.array``   |
        +-------------------------------------------+

        """
        blue, green, red = cv2.split(image)

        blueProcessed = apply_mask(self.kernel, blue)
        greenProcessed = apply_mask(self.kernel, green)
        redProcessed = apply_mask(self.kernel, red)

        #  merge all channels back
        image_processed = cv2.merge((blueProcessed, greenProcessed, redProcessed))
        return image_processed

    def gray_process(self, image):
        """
        \n**General description of the method:**

        \nIt convert the input image in gray scale in order to find its edges. Then compute the convolution between the image and the kernel through :meth:`apply_mask`

        +---------------------------------------+
        |**Input**: *image*                     |
        +=======================================+
        |  **image type:** ``np.array``         |
        +---------------------------------------+

        +--------------------------------------------+
        |**Output**: *processed_image*               |
        +============================================+
        |  **processed_image type:** ``np.array``    |
        +--------------------------------------------+
        """
        image = np.array(image, dtype=np.uint8)  # required to prevent errors in the next row.
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        processed_image_gray = apply_mask(self.kernel, image)
        processed_image = cv2.merge((processed_image_gray, processed_image_gray, processed_image_gray))

        return processed_image

    @abstractmethod
    def kernel_generator(self):
        """
        \nkernel_generator is an abstract method. It is called by the init and should return a numpy array with dimension [self.kernel_dimension x self.kernel_dimension].
        \nIts subclasses generate a different kernel according to their class and their aim.

        """
        pass


class BlurFilter(Filters):
    """
    \nBlurFilter is a subclass of :class:`.Filters`, applying this filter to a image it makes the image blurred
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nIt generates a kernel made to blur an image.
        \nThe kernel generated is composed by a matrix of ones with dimension [self.kernel_dimensio, self.kernel_dimensio].

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """

        return np.ones((self.kernel_dimension, self.kernel_dimension))


class BoxBlurFilter(Filters):
    """
    BoxBlurFilter is a subclass of :class:`.Filters`, applying this filter to a image it makes the image blurred
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nIt generates a kernel made to blur an image.
        \nThe kernel generated is composed by a matrix of ones, with dimension [self.kernel_dimension, self.kernel_dimension] divided by the number of the elements.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        return (1 / self.kernel_dimension ** 2) * np.ones((self.kernel_dimension, self.kernel_dimension))


class GaussianBlurFilter(Filters):
    """
    \nGaussianBlurFilter is a subclass of :class:`.Filters`, applying this filter to a image it makes the image blurred
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nIt generates a kernel made to blur an image.
        \nThe kernel generated is based on a bidimensional version of the gaussian distribution.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        std = 3
        gaussian_vector = signal.gaussian(self.kernel_dimension, std=std).reshape(self.kernel_dimension, 1)
        kernel = np.outer(gaussian_vector, gaussian_vector)
        kernel = kernel / np.sum(kernel)
        return kernel


class EdgeDetectionFilter(Filters):
    """
    \nEdgeDetentionFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nThis method generates a kernel made to detect the edges.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        kernel = -1 * np.ones((self.kernel_dimension, self.kernel_dimension))
        kernel[int(self.kernel_dimension / 2), int(
            self.kernel_dimension / 2)] = self.kernel_dimension ** 2 - 1  # or another number
        return kernel

    def process(self, image):
        processed_image = self.gray_process(image)

        return processed_image


class HorizontalEdgeDetectionFilter(Filters):
    """
    \nHorizontalEdgeDetentionFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nThis method generates a kernel made to detect the edges.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        row = np.array(range(int(-self.kernel_dimension / 2), int(self.kernel_dimension / 2) + 1))
        kernel = np.zeros((self.kernel_dimension, self.kernel_dimension))
        for i in range(self.kernel_dimension):
            kernel[:, i] = row
        kernel[:, int(self.kernel_dimension / 2)] *= 2
        kernel = kernel.transpose()
        return kernel

    def process(self, image):
        processed_image = self.gray_process(image)

        return processed_image


class VerticalEdgeDetectionFilter(Filters):
    """
    \nHorizontalEdgeDetentionFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nThis method generates a kernel made to detect the edges.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        row = np.array(range(int(-self.kernel_dimension / 2), int(self.kernel_dimension / 2) + 1))
        kernel = np.zeros((self.kernel_dimension, self.kernel_dimension))
        for i in range(self.kernel_dimension):
            kernel[:, i] = row
        kernel[:, int(self.kernel_dimension / 2)] *= 2
        return kernel

    def process(self, image):
        processed_image = self.gray_process(image)

        return processed_image


class SharpenFilter(Filters):
    """
    SharpenFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        """
        **General description of the method:**

        This method generates a kernel made to detect the edges.

        +-----------------------------------+
        |**Output**: *kerenl*               |
        +===================================+
        |  **kernel type:** ``np.array``    |
        +-----------------------------------+
        """
        kernel = np.zeros((self.kernel_dimension, self.kernel_dimension))
        kernel[:, int(self.kernel_dimension / 2)] = -1 * np.ones(self.kernel_dimension)
        kernel[int(self.kernel_dimension / 2), :] = -1 * np.ones(self.kernel_dimension)
        kernel[int(self.kernel_dimension / 2), int(self.kernel_dimension / 2)] = 9  # or another number
        return kernel


class MedianFilter(Filters):
    """
    SharpenFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        return None

    def process(self, image):
        blue, green, red = cv2.split(image)

        blueProcessed = apply_median(self.kernel_dimension, blue)
        greenProcessed = apply_median(self.kernel_dimension, green)
        redProcessed = apply_median(self.kernel_dimension, red)

        #  merge all channels back
        image_processed = cv2.merge((blueProcessed, greenProcessed, redProcessed))
        return image_processed


class BinarizatorFilter(Filters):
    """
    SharpenFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        return None

    def process(self, image):
        image = np.array(image, dtype=np.uint8)  # required to prevent errors in the next row.
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        h, w = gray_image.shape
        max = np.max(gray_image)
        for i in range(h):
            for j in range(w):
                if gray_image[i, j] > max / 2:
                    gray_image[i, j] = 255
                else:
                    gray_image[i, j] = 0

        processed_image = cv2.merge((gray_image, gray_image, gray_image))
        return processed_image


class SobelEdgeFilter(Filters):
    """
    \nSobelEdgeFilter is a subclass of :class:`.Filters`, applying this filter to a image it underlines the edges.
    """

    def kernel_generator(self):
        """
        \n**General description of the method:**

        \nIt generates two kernels, one to detect the edges in vertical and one to detect the edges in horizontal. Then it return them as a list.

        +-----------------------------------+
        |**Output**: *kerenls*              |
        +===================================+
        |                                   |
        |  **kernels type:** ``list``       |
        +-----------------------------------+
        """
        kernels = []

        row = np.array(range(int(-self.kernel_dimension / 2), int(self.kernel_dimension / 2) + 1))
        kernel_y = np.zeros((self.kernel_dimension, self.kernel_dimension))
        for i in range(self.kernel_dimension):
            kernel_y[:, i] = row
        kernel_y[:, int(self.kernel_dimension / 2)] *= 2

        kernel_x = kernel_y.transpose()

        kernels.append(kernel_x)
        kernels.append(kernel_y)

        return kernels

    def process(self, image):
        """
        \n**General description of the method:**

        \nIt works similar to :meth:`.Filters.process`.
        \nIt splits the image into three layer, one for each color.
        \nIt computes the convolution two times for each color layer, using the the kernel generates by :meth:`SobelEdgeFilter.kernel_generator`.
        \nThen it merges before each couple of matrices generated by each layer and finally, it merges the three layers in order to recompose the image.
        \nIt returns the image computed.

        +---------------------------------------+
        |**Input**: *image*                     |
        +=======================================+
        |  **image type:** ``np.array``         |
        +---------------------------------------+

        +---------------------------------------+
        |**Output**: *rgb_edge*                 |
        +=======================================+
        |                                       |
        |  **rgb_edge_type type:** ``np.array`` |
        +---------------------------------------+
        """

        h, w, d = image.shape
        # define images with 0s
        newgradientImage = np.zeros((h, w, d))

        # offset by 1
        for channel in range(d):
            for i in range(1, h - 1):
                for j in range(1, w - 1):
                    horizontalGrad = convolution_product(self.kernel[1], image[i - 1:i + 2, j - 1:j + 2, channel])
                    verticalGrad = convolution_product(self.kernel[0], image[i - 1:i + 2, j - 1:j + 2, channel])

                    # Edge Magnitude
                    mag = np.sqrt(horizontalGrad ** 2 + verticalGrad ** 2)
                    # Avoid underflow: clip result
                    newgradientImage[i - 1, j - 1, channel] = mag

        # now add the images r g and b
        gray_image = newgradientImage[:, :, 0] + newgradientImage[:, :, 1] + newgradientImage[:, :, 2]

        image_processed = cv2.merge((gray_image, gray_image, gray_image))

        return image_processed


def convolution_product(x1, x2):
    """
    **General description of the method:**

    It takes as input two matrices, it multiplies each element of the first matrices to the element of the second matrix that has the same position, then it calculates the summation of all of them and returns it.\n



        +-------------------------------+
        |**Input**: *x1*, *x2*          |
        +===============================+
        |                               |
        |  **x1 type:** ``np.array``    |
        |                               |
        |  **x2 type:** ``np.array``    |
        +-------------------------------+

        +-------------------------------+
        |**Output**: *output*           |
        +===============================+
        |                               |
        |  **output type:** ``int``     |
        +-------------------------------+
    """
    if np.shape(x1) == np.shape(x2):
        x = np.shape(x1)[0]
        y = np.shape(x1)[1]
        output = 0
        for x_el in range(x):
            for y_el in range(y):
                output += x1[x_el, y_el] * x2[x_el, y_el]
        return output


def apply_mask(kernel, img):
    """
    \n**General description of the method:**

    \nIt takes as input two matrices, then it computes the convolution between them.


        +-----------------------------------+
        |**Input**: *kernel*, *img*         |
        +===================================+
        |                                   |
        |  **kernel type:** ``np.array``    |
        |                                   |
        |  **img type:** ``np.array``       |
        +-----------------------------------+

        +-------------------------------+
        |**Output**: *img*              |
        +===============================+
        |                               |
        |  **img type:** ``np.array``   |
        +-------------------------------+
    """
    image_height = img.shape[0]
    image_width = img.shape[1]
    kernel_dimension = kernel.shape[0]
    frame_size = int(kernel_dimension / 2)

    temporaly_img = np.zeros(np.array(img.shape) + (frame_size * 2, frame_size * 2))
    temporaly_img[frame_size:image_height + frame_size, frame_size:image_width + frame_size] = img

    for i in range(frame_size):
        temporaly_img[:, i] = temporaly_img[:, frame_size]
        temporaly_img[i, :] = temporaly_img[frame_size, :]

        temporaly_img[:, temporaly_img.shape[1] - 1 - i] = temporaly_img[:, temporaly_img.shape[1] - frame_size - 1]
        temporaly_img[temporaly_img.shape[0] - 1 - i, :] = temporaly_img[temporaly_img.shape[0] - frame_size - 1, :]

    for i in range(image_height):
        for j in range(image_width):
            img[i, j] = convolution_product(kernel, temporaly_img[i:i + kernel_dimension, j:j + kernel_dimension])

    return img


def apply_median(kernel_dimension, image):
    """
    \n**General description of the method:**

    \nIt takes as input two matrices, then it computes the convolution between them.


        +------------------------------------------+
        |**Input**: *kernel*, *img*                |
        +==========================================+
        |                                          |
        |  **kernel_dimension type:** ``int``      |
        |                                          |
        |  **img type:** ``np.array``              |
        +------------------------------------------+

        +-------------------------------+
        |**Output**: *image*            |
        +===============================+
        |                               |
        |  **image type:** ``np.array`` |
        +-------------------------------+
    """
    image_height = image.shape[0]
    image_width = image.shape[1]
    frame_size = int(kernel_dimension / 2)

    temporaly_img = np.zeros(np.array(image.shape) + (frame_size * 2, frame_size * 2))
    temporaly_img[frame_size:image_height + frame_size, frame_size:image_width + frame_size] = image

    for i in range(frame_size):
        temporaly_img[:, i] = temporaly_img[:, frame_size]
        temporaly_img[i, :] = temporaly_img[frame_size, :]

        temporaly_img[:, temporaly_img.shape[1] - 1 - i] = temporaly_img[:, temporaly_img.shape[1] - frame_size - 1]
        temporaly_img[temporaly_img.shape[0] - 1 - i, :] = temporaly_img[temporaly_img.shape[0] - frame_size - 1, :]

    for i in range(image_height):
        for j in range(image_width):
            image[i, j] = np.median(temporaly_img[i:i + kernel_dimension, j:j + kernel_dimension])

    return image
