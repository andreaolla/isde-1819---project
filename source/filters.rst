Filters
=====================================

Filters class belongs to Controller module, it takes care of the processing of the images.
In this module the image is processed through a convolution between the image and a specific kernel.
Each filter has its own kernel that is generated dynamically. It is possible to generate kernels with chosen dimension.

Each filter is a subclass of :class:`.Filters`.



.. automodule:: controller.filters
   :members:
   :undoc-members:
   :show-inheritance: