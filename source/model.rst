Model
=====================================

The model_state class permits to relegate to the Controller the execution of actions decided by the user.
\nIt uses state pattern to differentiate the menu behavior according to the program status.


.. automodule:: model.model_state
   :members:
   :undoc-members:
   :show-inheritance: