.. Isde_1891 documentation master file, created by
   sphinx-quickstart on Tue Mar 12 19:48:09 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root toctree directive.

Isde_1891_project documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   controller
   model
   view
   filters
   model_fun_utils



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`